package io.gitlab.arkdirfe.damagetestutil;

import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabExecutor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class Commands implements TabExecutor
{
    private final DamageTestUtil plugin;

    public Commands(@NotNull final DamageTestUtil plugin, @NotNull final String commandName)
    {
        this.plugin = plugin;
        PluginCommand cmd = plugin.getCommand(commandName);
        if(cmd != null)
        {
            cmd.setExecutor(this);
            cmd.setTabCompleter(this);
        }
        else
        {
            plugin.getLogger().severe("Unable to register commands!");
        }
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args)
    {
        Player player;

        if(sender instanceof Player)
        {
            player = (Player) sender;
        }
        else
        {
            sender.sendMessage("Commands not supported from console!");
            return false;
        }

        if(args.length > 0)
        {
            String subCmd = args[0];

            if(subCmd.equalsIgnoreCase("help"))
            {
                player.sendMessage("Commands:");
                player.sendMessage(StringUtil.formatLine("   #aaaa44help#ffffff: Displays this page."));
                player.sendMessage(StringUtil.formatLine("   #aaaa44hitme#aaaaaa [damage]#ffffff: Simulates a single hit."));
                player.sendMessage(StringUtil.formatLine("   #aaaa44range#aaaaaa [increment] [max]#ffffff: Simulates incremental amounts of damage."));
                player.sendMessage(StringUtil.formatLine("   #aaaa44gear#aaaaaa [armor] [toughness] [protection]#ffffff: Provides a Netherite Chestplate with the requested stats."));
                return true;
            }
            else if(subCmd.equalsIgnoreCase("hitme") && args.length > 1)
            {
                double damage;

                try
                {
                    damage = Double.parseDouble(args[1]);
                }
                catch(NumberFormatException e)
                {
                    return false;
                }

                plugin.playersRequesting.add(player.getUniqueId());
                Entity fakeCreeper = player.getWorld().spawnEntity(player.getLocation(), EntityType.CREEPER);
                player.damage((damage - 2) * 2 + 2, fakeCreeper); // Something weird happens if I use an entity so I need to correct the calculation.
                fakeCreeper.remove();
                plugin.playersRequesting.remove(player.getUniqueId());
                return true;
            }
            else if(subCmd.equalsIgnoreCase("range") && args.length > 2)
            {
                double increment;
                double max;

                try
                {
                    increment = Double.parseDouble(args[1]);
                    max = Double.parseDouble(args[2]);
                }
                catch(NumberFormatException e)
                {
                    return false;
                }

                double health = player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();

                plugin.playersRequesting.add(player.getUniqueId());
                Entity fakeCreeper = player.getWorld().spawnEntity(player.getLocation(), EntityType.CREEPER);
                for(double damage = increment; damage <= max; damage += increment)
                {
                    player.damage((damage - 2) * 2 + 2, fakeCreeper); // Something weird happens if I use an entity so I need to correct the calculation.
                }
                fakeCreeper.remove();
                plugin.playersRequesting.remove(player.getUniqueId());
                return true;
            }
            else if(subCmd.equalsIgnoreCase("gear") && args.length > 3)
            {
                int armor;
                int toughness;
                int protection;

                try
                {
                    armor = Integer.parseInt(args[1]);
                    toughness = Integer.parseInt(args[2]);
                    protection = Integer.parseInt(args[3]);
                }
                catch(NumberFormatException e)
                {
                    return false;
                }

                ItemStack gear = new ItemStack(Material.NETHERITE_CHESTPLATE);
                gear.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, protection);
                ItemMeta meta = gear.getItemMeta();
                meta.addAttributeModifier(Attribute.GENERIC_ARMOR, new AttributeModifier(UUID.randomUUID(), "", armor, AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.CHEST));
                meta.addAttributeModifier(Attribute.GENERIC_ARMOR_TOUGHNESS, new AttributeModifier(UUID.randomUUID(), "", toughness, AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.CHEST));
                gear.setItemMeta(meta);
                player.getInventory().addItem(gear);
                return true;
            }
        }

        return false;
    }

    @Nullable
    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args)
    {
        if(args.length < 2)
        {
            return Arrays.asList("help", "hitme", "range", "gear");
        }

        return new ArrayList<>();
    }
}
