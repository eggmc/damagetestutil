package io.gitlab.arkdirfe.damagetestutil;

import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class DamageTestUtil extends JavaPlugin
{
    public List<UUID> playersRequesting;

    @Override
    public void onEnable()
    {
        getLogger().warning("DO NOT USE THIS PLUGIN ON A NON-TEST SERVER! IT IS NOT PERMISSION LOCKED AND ANYONE WILL BE ABLE TO RUN ITS COMMANDS!");
        new Commands(this, "damagetestutil");
        new Listeners(this);
        playersRequesting = new ArrayList<>();
    }

    @Override
    public void onDisable()
    {
        playersRequesting.clear();
    }
}
