package io.gitlab.arkdirfe.damagetestutil;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class Listeners implements Listener
{
    private DamageTestUtil plugin;

    public Listeners(final DamageTestUtil plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plugin = plugin;
    }

    @EventHandler
    public void onEntityDamaged(final EntityDamageEvent event)
    {
        Player player;

        if(event.getEntityType() != EntityType.PLAYER)
        {
            return;
        }
        else
        {
            player = (Player) event.getEntity();
        }

        if(!plugin.playersRequesting.contains(player.getUniqueId()))
        {
            return;
        }

        // Big ugly line, cry about it
        player.sendMessage(StringUtil.formatLine(String.format("Raw: %.2f %s Effective: %.2f %s", event.getDamage(), event.getDamage() >= 10 ? event.getDamage() >= 100 ? "" : "#000000-#ffffff" : "#000000--#ffffff", event.getFinalDamage(), event.getFinalDamage() >= player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue() ? "#aa4444(Lethal)" : "")));
        event.setCancelled(true);
    }
}
