package io.gitlab.arkdirfe.damagetestutil;

import net.md_5.bungee.api.ChatColor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class StringUtil
{
    private static final Pattern hexPattern = Pattern.compile("#[a-fA-F0-9]{6}");
    private static final Pattern formatPattern = Pattern.compile("§[a-f0-9klmnorx]");
    private static final String defaultColor = "#ffffff";

    /**
     * Formats the hex codes.
     *
     * @param string String with hex codes.
     * @return Formatted string.
     */
    @NotNull
    private static String formatHex(@NotNull String string)
    {
        Matcher match = hexPattern.matcher(string);

        while(match.find())
        {
            String color = string.substring(match.start(), match.end());
            string = string.replace(color, ChatColor.of(color) + "");
            match = hexPattern.matcher(string);
        }
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    /**
     * Formats a single line.
     *
     * @param line The line to format.
     * @return Formatted line.
     */
    @NotNull
    public static String formatLine(@NotNull final String line)
    {
        return "§r" + formatHex(defaultColor + line);
    }

    /**
     * Splits a string at \n and formats the lines.
     *
     * @param string String to process.
     * @return List of lines.
     */
    @NotNull
    public static List<String> splitAndFormatLines(@NotNull final String string)
    {
        List<String> lines = new ArrayList<>();

        for(String s : string.split("\n"))
        {
            lines.add(formatLine(s));
        }

        return lines;
    }
}
